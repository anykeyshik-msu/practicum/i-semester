#!/usr/bin/env python3

with open("out", "r") as out:
    lines = out.readlines()
    i = 0

    for line in lines:
        if i < 3:
            i += 1
            continue
        print(line[:-1], end=': ')
        print(len(line[:-1]))
