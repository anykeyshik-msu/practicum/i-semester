#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char * inputString(void);

int 
main(void) 
{
    long int n;
    int len, count;
    char *s, *new;
    char newline;

    scanf("%ld%c", &n, &newline);
    s = inputString();
    new = calloc(strlen(s) + 1, sizeof(char));

    len = strlen(s) - 1;
    n %= len;
    count = 0;
    
    for (long int i = n; i < len; i++) {
        new[count] = s[i];
        count++;
    }
    for (long int i = 0; i < n; i++) {
        new[count] = s[i];
        count++;
    }

    printf("%s", new);

    free(new);
    free(s);

    return 0;
}

char *
inputString(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
                return str;
    }

    while (EOF != (ch = fgetc(stdin)) && ch != '\n') {
        str[len++] = ch;

        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char)*len);
}

