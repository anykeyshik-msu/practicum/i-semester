#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * inputString(void);

int
main(void)
{
    int i;
    char *str;
    
    str = inputString();

    for (int t = 1; t <= strlen(str); t++) {
        for (i = t; i < strlen(str); i++) {
            if (str[i] != str[i - t]) {
                break;
            }
        }

        if (i == strlen(str)) {
            printf("%d ", t);
        }
    }

    free(str);

    return 0;
}

char *
inputString(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
		return str;
    }
    
    while (EOF != (ch = fgetc(stdin)) && ch != '\n') {
        str[len++] = ch;
        
        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char)*len);
}
