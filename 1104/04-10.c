#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int findPrefix(char *, char *);
char * inputString(void);

int
main(void)
{
    char *str1;
    char *str2;

    str1 = inputString();
    str2 = inputString();

    printf("%d %d", findPrefix(str2, str1), findPrefix(str1, str2));

    free(str1);
    free(str2);

    return 0;
}

int
findPrefix(char *str1, char *str2)
{
    int first_len, second_len, j;
    int *pi, max;

    first_len = strlen(str1);
    second_len = strlen(str2);

    pi = (int *)calloc(first_len, sizeof(int));

    j = pi[0];
    max = pi[0];
    for (int i = 1; i < first_len && j < second_len; i++) {
		j = pi[i - 1];

        while (j > 0 && str1[i] != str2[j]) {
			j = pi[j - 1];
        }
		
        if (str1[i] == str2[j]) {
            j++;
        }

		pi[i] = j;
	}

#if DEBUG
    fprintf(stderr, "\n");
    fprintf(stderr, "==> First string: %s\n", str1);
    fprintf(stderr, "==> Second string: %s\n", str2);
    for (int i = 0; i < first_len; i++) {
        fprintf(stderr, "==> pi[%d] = %d\n", i, pi[i]);
    }
#endif

    max = pi[first_len - 1];

    free(pi);

    return max;
}

char *
inputString(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
                return str;
    }

    while (EOF != (ch = fgetc(stdin)) && ch != '\n') {
        str[len++] = ch;

        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char)*len);
}
