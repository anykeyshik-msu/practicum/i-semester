#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *
input_string(void);

char **
split_string(char *, int *);

int
main(void)
{
    char *input;
    char **splitted;
    int len;
    int out_size;
    int curr_len;

    input = calloc(1, sizeof(char));
    scanf("%d", &out_size);
    scanf("%c", input);
    free(input);

    input = input_string();

    splitted = split_string(input, &len);

    curr_len = 0;
    for (int i = 0; i < len; i++) {
        curr_len += strlen(splitted[i]);
        printf("%s", splitted[i]);

        int words_count = i + 1;

        while (words_count < len) {
            if (curr_len + strlen(splitted[words_count]) + 1 < out_size) {
                curr_len += strlen(splitted[words_count]) + 1;
                words_count++;
            } else {
                break;
            }
        }

        int all_spaces = out_size + curr_len - (words_count - i - 1);
        int spaces = all_spaces;
        int remainder = 0;
        if (words_count - i - 1 != 0) {
            spaces = all_spaces / (words_count - i - 1);
            remainder = all_spaces % (words_count - i - 1);
        }

        for (int j = i + 1; j < words_count; j++) {
            for (int k = 0; k < spaces; k++) {
                printf(" ");
            }
            curr_len += spaces;
            if (remainder != 0) {
                printf(" ");
                remainder--;
                curr_len++;
            }

            printf("%s", splitted[j]);
        } 

        for (int j = curr_len; j < out_size; j++) {
            printf(" ");
        }

        printf("\n");
        curr_len = 0;
        i += words_count - i - 1;
    }
    
    free(splitted);
    free(input);

    return 0;
}

char **
split_string(char *str, int *len)
{
    char **res;
    char *token;
    size_t idx;
    size_t size;

    idx = 0;
    size = 20;

    res = calloc(size, sizeof(char *));
    if (!res) {
        return res;
    }

    token = strtok(str, " ");
    while (token)
    {
        res[idx++] = token;
        token = strtok(NULL, " ");

        if (idx == size) {
            size *= 2;
            res = realloc(res, sizeof(char *) * size);
        }
    }

    free(token);

    *len = idx;
    return realloc(res, sizeof(char *) * idx);
}

char *
input_string(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
        return str;
    }

    while (EOF != (ch = fgetc(stdin)) && ch != '\n') {
        str[len++] = ch;

        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char) * len);
}
