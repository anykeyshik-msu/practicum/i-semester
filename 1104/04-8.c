#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STR_SIZE 105
#define ARR_SIZE 10

int 
main(void) 
{
    int numbers[10][10][10] = {0};
    char *s;
    int len, unique;

    s = calloc(STR_SIZE, sizeof(char));
    fgets(s, 105, stdin);
    len = strlen(s) - 1;
    
    for (int i = 0; i < len; i++) {
        for (int j = i; j < len; j++) {
            for (int k = j; k < len; k++) {
                if ((i != j) && (j != k) && (i != k)) {
                    if (s[i] != 0) {
                        numbers[s[i]-48][s[j]-48][s[k]-48] += 1;
                    }
                }
            }
        }
    }
    unique = 0;
    for (int i = 1; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            for (int k = 0; k < 10; k++) {
                if (numbers[i][j][k] != 0) {
                    unique++;
                } 
            }
        }
    }

    printf("%d ", unique);

    free(s);

    return 0;
}
