#include <stdint.h>
#include <stdio.h>

int main(void) {
    uint32_t n;
    uint32_t sum = 0;

    scanf("%u", &n);

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= n; j++) {
            sum += i * j;
        }
    }

    printf("%u\n", sum);
}
