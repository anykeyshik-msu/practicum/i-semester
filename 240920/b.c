#include <stdint.h>
#include <stdio.h>

int main(void) {
    int16_t a, b;

    scanf("%hd %hd", &a, &b);

    printf("%hd\n", a + b);
}
