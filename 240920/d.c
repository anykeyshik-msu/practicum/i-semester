#include <stdio.h>

int min(int, int);
int max(int, int);

int main(void) {
    int minV, maxV, minI, maxI, n;

    scanf("%u", &n);

    int array[n];
    scanf("%u", &array[0]);

    minV = array[0];
    minI = 0;
    maxV = array[0];
    maxI = 0;

    for(int i = 1; i < n; i++) {
        scanf("%u", &(array[i]));
        if(array[i] < minV) {
            minV = array[i];
            minI = i;
        }

        if(array[i] > maxV) {
            maxV = array[i];
            maxI = i;
        }
    }

    fprintf(stderr, "-----> min = %d max = %d\n\n", min(minI, maxI), max(minI, maxI));

    for(int i = 0; i < min(minI, maxI); i++) {
        printf("%u ", array[i]);
        fprintf(stderr, "1: ===> i = %d\n", i);
    }
    for(int i = max(minI, maxI); i >= min(minI, maxI); i--) {
        printf("%u ", array[i]);
        fprintf(stderr, "2: ===> i = %d\n", i);
    }
    for(int i = max(minI, maxI) + 1; i < n; i++) {
        printf("%u ", array[i]);
        fprintf(stderr, "3: ===> i = %d\n", i);
    }
}

int min(int a, int b) {
    return a < b ? a : b;
}

int max(int a, int b) {
    return a > b ? a : b;
}
