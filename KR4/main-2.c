#include "k4-2.h"
#include <stdio.h>

int min_int(int, int);

int
main(void)
{
    struct list *a;
    struct list *prev_a;
    struct list *b;
    struct list *prev_b;

    struct list *head_a;
    struct list *head_b;
    struct list *ans;

    int FLEN, SLEN;

    printf("Size of first list:\n");
    scanf("%d", &FLEN);
    printf("Size of second list:\n");
    scanf("%d", &SLEN);

    a = calloc(1, sizeof(struct list));
    b = calloc(1, sizeof(struct list));
    head_a = a;
    head_b = b;
    prev_a = a;
    prev_b = b;

    for (int i = 0; i < FLEN; i++) {
        printf("First list\n");
        scanf("%d", &a->number);

        a->next = calloc(1, sizeof(struct list));
        prev_a = a;
        a = a->next;
    }
    prev_a->next = NULL;
    free(a);
    for (int i = 0; i < SLEN; i++) {
        printf("Second list\n");
        scanf("%d", &b->number);

        b->next = calloc(1, sizeof(struct list));
        prev_b = b;
        b = b->next;
    }
    prev_b->next = NULL;
    free(b);

    if (FLEN == 0) {
        head_a = NULL;
    }
    if (SLEN == 0) {
        head_b = NULL;
    }

    ans = combine(head_a, head_b);

    while (ans != NULL) {
        printf("number = %d\tnext = %p\n", ans->number, ans->next);
        ans = ans->next;
    }

    free(ans);

    return 0;
}

int
min_int(int a, int b)
{
    if (a < b) {
        return b;
    }
    
    return a;
}
