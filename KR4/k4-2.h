#include <stdlib.h>
#include <math.h>

struct list
{
	int number;
	struct list *next;
};

struct list *
combine(struct list *a, struct list *b)
{
    if (a != NULL || b != NULL) {
        struct list *ans;
        struct list *curr;
        struct list *prev;
        int i;

        curr = calloc(1, sizeof(struct list));
        ans = curr;
        prev = NULL;
        i = 0;

        while (a != NULL && b != NULL) {
            if ( abs(abs(a->number)) - i < abs(abs(b->number) - i) ) {
                curr->number = a->number;
            } else if ( abs(abs(a->number)) - i == abs(abs(b->number) - i) ) {
                if (a->number < b->number) {
                    curr->number = a->number;
                } else {
                    curr->number = b->number;
                }
            } else {
                curr->number = b->number;
            }

            curr->next = calloc(1, sizeof(struct list));
            prev = curr;
            curr = curr->next;
            a = a->next;
            b = b->next;

            i++;
        }

        while (a != NULL) {
            curr->number = a->number;

            curr->next = calloc(1, sizeof(struct list));
            prev = curr;
            curr = curr->next;
            a = a->next;
        }

        while (b != NULL) {
            curr->number = b->number;

            curr->next = calloc(1, sizeof(struct list));
            prev = curr;
            curr = curr->next;
            b = b->next;
        }

        prev->next = NULL;
        free(curr);

        return ans;
    }

    return NULL;
}
