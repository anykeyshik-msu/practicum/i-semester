#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char * inputString(void);

int
main(void)
{
    char *str;
    int len_str;
    int part;

    str = inputString();
    len_str = strlen(str);

    for (int i = 0; i < 6; i++) {
        scanf("%d", &part);

        printf( "%.*s", (int)(len_str / 7), str + (part - 1) * (int)(len_str / 7) );
    }

    free(str);

    return 0;
}

char *
inputString(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
                return str;
    }

    while (EOF != (ch = fgetc(stdin)) && ch != '\n') {
        str[len++] = ch;

        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char)*len);
}
