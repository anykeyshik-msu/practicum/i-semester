def gen(str, s, z):
    if s < 0 and z < 0:
        return ""

    print(s, z)

    if s >= 0:
        str += gen(str, s - 1, z)
    if z >= 0:
        str += gen(str, s, z - 1)

    if s == 1:
        str += 'S'
        str += gen(str, 0, z)

    if z == 1:
        str += 'Z'
        str += gen(str, s, 0)

    return str

n = int(input())
k = int(input())
str = ""

print(gen(str, n - k, k))
