#include <stdio.h>

int count(unsigned int);

int
main(void) 
{
    unsigned int min = 0;
    unsigned int input;

    do {
        scanf("%u", &input);

        if ( (count(min) < count(input)) || (count(min) == count(input) && min > input) ) {
            min = input;
        }
    } while (input != 0);

    printf("%u", min);

    return 0;
}

int 
count(unsigned int number)
{
    int count = 0;

    while (number != 0) {
        if (number % 2 == 1) {
            count++;
        }
        number >>= 1;
    }

    return count;
}
