#include <stdio.h>

int gcd(int, int);

int
main(void)
{
    int a1, b1;
    int a2, b2;
    int numer, denumer;
    int n;

    scanf("%d", &n);
    scanf("%d %d", &a1, &b1);
    scanf("%d %d", &a2, &b2);

    numer = (a1 * b2) + (b1 * a2);
    denumer = 1;
    if (numer != 0) {
        denumer = b1 * b2;
        int temp = gcd(numer, denumer);

        numer /= temp;
        denumer /= temp;
    }

    for (int i = 2; i < n; i++) {
        a2 = numer;
        b2 = denumer;

        scanf("%d %d", &a1, &b1);

        numer = (a1 * b2) + (b1 * a2);
        denumer = 1;
        if (numer != 0) {
            denumer = b1 * b2;
            int temp = gcd(numer, denumer);

            numer /= temp;
            denumer /= temp;
        }
    }

    printf("%d %d %d", numer / denumer, numer - ((numer / denumer) * denumer), denumer);

    return 0;
}

int
gcd(int a, int b)
{
    while ( (a != 0) && (b != 0) ) {
        if(a > b) {
            a %= b;
        } else {
            b %= a;
        }
    }

    return a + b;
}
