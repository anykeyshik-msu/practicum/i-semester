#include <stdio.h>

int vertex;

int check(int);

int
main(void)
{
    int n;
    int a, b;

    scanf("%d", &n);
    scanf("%d %d", &a, &b);
    vertex = 1;

    if (check(n)) {
        printf("Yes");
    } else {
        printf("No");
    }
}

int 
check(int n) 
{
    int a1, b1;
    int a2, b2;

    vertex++;

    if (vertex == (n / 2) + 1) {
        scanf("%d %d", &a1, &b1);

        return 1;
    }

    scanf("%d %d", &a1, &b1);
    if (vertex <= (n / 2) + 1) {
        if (!check(n)) {
            return 0;
        }
    }

    scanf("%d %d", &a2, &b2);
    if ( (a1 * (-1) == a2) && (b1 == b2) ) {
        return 1;
    }

    return 0;
}
