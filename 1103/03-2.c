#include <stdio.h>
#include <math.h>

int isPrime(int);

int 
main(void)
{
    int number;
    
    scanf("%d", &number);

    while (!isPrime(number)) {
        number++;
    }

    printf("%d", number);

    return 0;
}

int 
isPrime(int number)
{
    if (number == 1) {
        return 0;
    }

    for (int i = 2; i <= sqrt(number); i++) {
        if ((number % i) == 0) {
            return 0;
        }
    }

    return 1;
}
