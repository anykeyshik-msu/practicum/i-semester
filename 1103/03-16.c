#include <stdio.h>

int new_pow(int, int);
int module(int, int, int);

int
main(void)
{
    int a, b, c;
    int m;
    int max;

    scanf("%d %d %d %d", &a, &b, &c, &m);
    max = -1;

    if (module(a, new_pow(b, c), m) > max) {
        max = module(a, new_pow(b, c), m);
    }
    if (module(a, new_pow(c, b), m) > max) {
        max = module(a, new_pow(c, b), m);
    }
    if (module(b, new_pow(a, c), m) > max) {
        max = module(b, new_pow(a, c), m);
    }
    if (module(b, new_pow(c, a), m) > max) {
        max = module(b, new_pow(c, a), m);
    }
    if (module(c, new_pow(a, b), m) > max) {
        max = module(c, new_pow(a, b), m);
    }
    if (module(c, new_pow(b, a), m) > max) {
        max = module(c, new_pow(b, a), m);
    }

    printf("%d", max);

    return 0;
}

int 
new_pow(int a, int b)
{
    if (b == 0) {
        return 1;
    }
    if (b == 1) {
        return a;
    }

    if (b % 2 == 0) {
        int temp = new_pow(a, b / 2);
        return temp * temp;
    } else {
        int temp = new_pow(a, (b - 1) / 2);
        return temp * temp * a;
    }
}

int 
module(int a, int b, int m)
{
    if (b == 0) {
        return 1;
    }
    if (b == 1) {
        return a % m;
    }

    if (b % 2 == 0) {
        int temp = module(a, b / 2, m);
        return (temp * temp) % m;
    } else {
        int temp = module(a, (b - 1) / 2, m);
        return (temp * temp * a) % m;
    }
}
