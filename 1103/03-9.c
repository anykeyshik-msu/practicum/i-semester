#include <stdio.h>
#include <math.h>

typedef struct point 
{
    double x;
    double y;
} point_t;

point_t getBisectCoords(point_t, point_t, point_t);
double getLength(double, double, double, double);

int
main(void)
{
    point_t a;
    point_t b;
    point_t c;
    point_t ans;

    scanf("%lf %lf", &a.x, &a.y);
    scanf("%lf %lf", &b.x, &b.y);
    scanf("%lf %lf", &c.x, &c.y);

    ans = getBisectCoords(a, b, c);
    printf("%.5lf %.5lf\n", ans.x, ans.y);
    ans = getBisectCoords(b, c, a);
    printf("%.5lf %.5lf\n", ans.x, ans.y);
    ans = getBisectCoords(c, a, b);
    printf("%.5lf %.5lf\n", ans.x, ans.y);
    
    return 0;
}

point_t
getBisectCoords(point_t a, point_t b, point_t c)
{
    point_t ans;

    ans.x = b.x + (a.x - b.x) * getLength(b.x, b.y, c.x, c.y) 
        / (getLength(a.x, a.y, c.x, c.y) + getLength(b.x, b.y, c.x, c.y)); 
    ans.y = b.y + (a.y - b.y) * getLength(b.x, b.y, c.x, c.y) 
        / (getLength(a.x, a.y, c.x, c.y) + getLength(b.x, b.y, c.x, c.y));

    return ans;
}

double
getLength(double x1, double y1, double x2, double y2)
{
    double xx = (x2 - x1) * (x2 - x1);
    double yy = (y2 - y1) * (y2 - y1);

    return sqrt(xx + yy);
}
