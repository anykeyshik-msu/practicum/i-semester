#include <stdio.h>
#include <stdlib.h>

void genSeq(int, int);

int
main(void)
{
    int n;
    int k;

    scanf("%d %d", &n, &k);

    genSeq(n, k);

    return 0;
}

void
genSeq(int n, int k)
{
    int *seq;
    seq = (int *)malloc(k * sizeof(int));

    for (int i = 0; i < k; i++) {
        seq[i] = i;
    }

    while (1) {
        for (int i = 0; i < k; i++) {
            printf("%d ", seq[i]);
        }
        printf("\n");

        if (seq[k - 1] < n - 1) {
            seq[k - 1]++;
        } else {
            int j;
            for (j = k - 1; j > 0; j--) {
                if (seq[j] - seq[j - 1] > 1) {
                    break;
                }
            }

            if (j == 0) {
                free(seq);
                break;
            }

            seq[j - 1]++;

            for (int i = j; i < k; i++) {
                seq[i] = seq[i - 1] + 1;
            }
        }
    }
}
