#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Domain
{
    unsigned int address;
    char name[101];
} Domain;

int 
domcmp(Domain *, Domain *);

int 
bin_search(Domain *, char *, int);

int
main(void)
{
    FILE *input;
    FILE *output;
    int n;
    int key_num;
    Domain *array;

    input = fopen("input.txt", "r");
    if (!input) {
        exit(-1);
    }
    output = fopen("output.txt", "w");
    if (!output) {
        exit(-1);
    }

    fscanf(input, "%d", &n);
    
    array = calloc(n, sizeof(Domain));
    for (int i = 0; i < n; i++) {
        fscanf(input, "%s", array[i].name);
        fscanf(input, "%u", &array[i].address);
    }
    qsort(array, n, sizeof(Domain), (int (*)(const void *, const void *))domcmp);

    fscanf(input, "%d", &key_num);
    for (int i = 0; i < key_num; i++) {
        char name[101];
        fscanf(input, "%s", name);

        int idx = bin_search(array, name, n);
        if (idx != -1) {
            fprintf(output, "%u\n", array[idx].address);
        } else {
            fprintf(output, "%d\n", idx);
        }
    }

    free(array);

    fclose(input);
    fclose(output);
}

int
domcmp(Domain *first, Domain *second)
{
    return strcmp(first->name, second->name);
}

int
bin_search(Domain *array, char *name, int n)
{
    int first, mid, last;

    first = 0;
    last = n;

    while (first <= last) {
        mid = (last + first) / 2;

        if (!strcmp(array[mid].name, name)) {
            return mid;
        }

        if (strcmp(array[mid].name, name) > 0) {
            last = mid - 1;
        } else {
            first = mid + 1;
        }
    }

    return -1;
}
