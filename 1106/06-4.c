#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NIL &empty

typedef enum colors
{
    RED,
 	BLACK
} node_color;

typedef struct Node
{
    struct Node *parent;
    struct Node *left;
    struct Node *right;

    node_color color;
    int key;
    int data;
} Node;

Node empty = {NIL, NIL, NIL, BLACK, 0, 0};

void
rotate_left(Node *, Node *);

void
rotate_right(Node *, Node *);

void
insert_fixup(Node *, Node *);

void
delete_fixup(Node *, Node *);

void
insert(Node *, int, int);

void
delete(Node *, int);

int
find(Node *, int);

void
erase(Node *);

int
main(void)
{
    Node *root;
    char op;
    int key;
    int data;

    root = NIL;
    scanf("%c", &op);

    if (op == 'A') {
        scanf("%d %d", &key, &data);
        insert(root, key, data);
    }
    if (op == 'D') {
        scanf("%d", &key);
        delete(root, key);
    }
    if (op == 'S') {
        scanf("%d", &key);
        int data = find(root, key);
        
        printf("%d %d\n", key, data);
    }
    if (op == 'F') {
        erase(root);
    }
}

void
rotate_left(Node *root, Node *node)
{
    Node *tmp = node->right;

    node->right = tmp->left;
    if (tmp->left != NIL) {
        tmp->left->parent = node;
    }

    if (tmp != NIL) {
        tmp->parent = node->parent;
    }
    if (node->parent) {
        if (node == node->parent->left) {
            node->parent->left = tmp;
        } else {
            node->parent->right = tmp;
        }
    } else {
        root = tmp;
    }

    tmp->left = node;
    if (node != NIL) {
        node->parent = tmp;
    }
}

void
rotate_right(Node *root, Node *node)
{
    Node *tmp = node->left;

    node->left = tmp->right;
    if (tmp->right != NIL) {
        tmp->right->parent = node;
    }

    if (tmp != NIL) {
        tmp->parent = node->parent;
    }
    if (node->parent) {
        if (node == node->parent->right) {
            node->parent->right = tmp;
        } else {
            node->parent->left = tmp;
        }
    } else {
        root = tmp;
    }

    tmp->left = node;
    if (node != NIL) {
        node->parent = tmp;
    }
}

void
insert_fixup(Node *root, Node *node)
{
    while (node != root && node->parent->color == RED) {
        if (node->parent == node->parent->parent->left) {
            Node *tmp = node->parent->parent->right;

            if (tmp->color == RED) {
                node->parent->color = BLACK;
                tmp->color = BLACK;
                node->parent->parent->color = RED;
                node = node->parent->parent;
            } else {
                if (node == node->parent->right) {
                    node = node->parent;
                    rotate_left(root, node);
                }

                node->parent->color = BLACK;
                node->parent->parent->color = RED;
                rotate_right(root, node->parent->parent);
            }
        } else {
            Node *tmp = node->parent->parent->left;

            if (tmp->color == RED) {
                nide->parent->color = BLACK;
                tmp->color = BLACK;
                node->parent->parent->color = RED;
                node = node->parent->parent;
            } else {
                if (node == node->parent->left) {
                    node = node->parent;
                    rotate_right(root, node);
                }

                node->parent->color = BLACK;
                node->parent->parent->color = RED;
                rotate_left(root, node->parent->parent);
            }
        }
    }

    root->color = BLACK;
}

void
delete_fixup(Node *root, Node *node)
{

}
