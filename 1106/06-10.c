#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    struct Node *prev;
    struct Node *next;
    int data;
} Node;

void
insert(Node **, int);

void
print(Node *, FILE *);

void
erase(Node *);

int
main(void)
{
    Node *root;
    Node *current;
    Node **array;
    int len, swaps;
    int start, end;
    FILE *input;
    FILE *output;

    input = fopen("input.txt", "r");
    if (!input) {
        exit(-1);
    }
    output = fopen("output.txt", "w");
    if (!output) {
        exit(-1);
    }

    root = calloc(1, sizeof(Node));
    root->data = 0;
    root->prev = NULL;
    root->next = NULL;
    current = root;

    fscanf(input, "%d %d", &len, &swaps);
    array = calloc(len, sizeof(Node *));

    array[0] = current;
    for (int i = 1; i < len; i++) {
        insert(&current, i);
        array[i] = current;
    }

    for (int i = 0; i < swaps; i++) {
        Node *start_prev;
        Node *end_next;
        
        fscanf(input, "%d %d", &start, &end);
        start--;
        end--;

        if (!array[start]->prev) {
            continue;
        }
        start_prev = array[start]->prev;

        if (array[end] && array[end]->next) {
            end_next = array[end]->next;
            start_prev->next = end_next;
            end_next->prev = start_prev;
        } else{
            start_prev->next = NULL;
        }

        root->prev = array[end];
        array[end]->next = root;
        root = array[start];
        root->prev = NULL;
    }

    print(root, output);

    erase(root);
    free(array);

    fclose(input);
    fclose(output);

    return 0;
}

void
insert(Node **node, int data)
{
    Node *tmp = calloc(1, sizeof(Node));

    tmp->data = data;
    tmp->prev = *node;
    tmp->next = NULL;

    (*node)->next = tmp;
    *node = tmp;
}

void
print(Node *root, FILE *output)
{
    while (root) {
        fprintf(output, "%d ", root->data + 1);
        root = root->next;
    }
}

void
erase(Node *node)
{
    while (node) {
        Node *tmp = node;
        node = node->next;
        free(tmp);
    }
}
