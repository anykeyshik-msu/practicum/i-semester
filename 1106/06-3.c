#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    struct Node *next;
    int data;
} Node;

Node *
insert(Node *, int);

Node *
sort(Node *);

void
print(Node *, FILE *);

void
erase(Node *);

int
main(void)
{
    Node *root;
    Node **current;
    FILE *input, *output;
    int num;

    input = fopen("input.txt", "r");
    if (input == NULL) {
        exit(-1);
    }
    output = fopen("output.txt", "w");
    if (output == NULL) {
        exit(-1);
    }

    root = NULL;
    current = &root;

    while (fscanf(input, "%d", &num) == 1) {
        *current = insert(*current, num);
        current = &(*current)->next;
    }
    
    root = sort(root);
    print(root, output);
    erase(root);

    fclose(input);
    fclose(output);

    return 0;
}

Node *
insert(Node *node, int data)
{
    Node *new = calloc(1, sizeof(Node));

    if (new) {
        new->data = data;

        if (node) {
            new->next = node->next;
            node->next = new;
        } else {
            new->next = NULL;
        }
    }

    return new;
}

Node *
sort(Node *root)
{
    Node *new_root = NULL;

    while (root) {
        Node *node = root;
        root = root->next;

        if (new_root == NULL || node->data < new_root->data) {
            node->next = new_root;
            new_root = node;
        } else {
            Node *current = new_root;

            while ( current->next && !(node->data < current->next->data) ) {
                current = current->next;
            }
            
            node->next = current->next;
            current->next = node;
        }
    }

    return new_root;
}

void
print(Node *node, FILE *output)
{
    for (; node; node = node->next) {
        fprintf(output, "%d ", node->data);
    }
}

void
erase(Node *node)
{
    while (node) {
        Node *tmp = node;
        node = node->next;
        free(tmp);
    }
}
