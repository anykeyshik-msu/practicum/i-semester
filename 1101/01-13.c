#include <stdio.h>

int main(void) {
    double x, powX, answer;
    int n;
    long long factN;

    scanf("%lf %d", &x, &n);
    powX = x;
    factN = 1;

    answer = powX / factN;
    for(int i = 1, j = 3; i < n; i++, j += 2) {
        powX *= x * x;
        factN *= j * (j - 1);

        fprintf(stderr, "%lf %lld\n", powX, factN);
        
        if(i % 2 == 0) {
            answer += powX / factN;
        }
        else {
            answer -= powX / factN;
        }
    }

    printf("%.6lf", answer);
}
