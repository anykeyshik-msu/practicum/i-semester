#include <stdio.h>
#include <limits.h>

int main(void) {
    int n, input;
    int max1 = INT_MIN;
    int max2 = INT_MIN;
    int max3 = INT_MIN;

    scanf("%d", &n);

    for(int i = 0; i < n; i++) {
        scanf("%d", &input);
        
        if(input > max1) {
            max3 = max2;
            max2 = max1;
            max1 = input;
        }
        else {
            if(input > max2) {
                max3 = max2;
                max2 = input;
            }
            else {
                if(input > max3) {
                    max3 = input;
                }
            }
        }
    }

    printf("%d %d %d", max1, max2, max3);
}
