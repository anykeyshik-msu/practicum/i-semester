#include <stdio.h>
#include <limits.h>

int main(void) {
    int input, n;
    int max = INT_MIN;
    int min = INT_MAX;

    scanf("%d", &n);

    for(int i = 0; i < n; i++) {
        scanf("%d", &input);

        if(input > max) {
            max = input;
        }

        if(input < min) {
            min = input;
        }
    }

    if(n < 2) {
        printf("0");
    } else {
        printf("%d", max - min);
    }
}
