#include <stdio.h>
#include <stdint.h>

int main(void) {
    uint32_t n, highN, lowN;
    uint8_t k;

    scanf("%u %hhu", &n, &k);

    lowN = n >> k;
    highN = n << (32 - k);

    printf("%u", lowN | highN);
}
