#include <stdio.h>

int main(void) {
    int result[4][2];
    unsigned int a11, a12, a21, a22, b1, b2, digit = 1;
    
    scanf("%u %u %u %u %u %u", &a11, &a12, &a21, &a22, &b1, &b2);
    
    for (int i = 0; i < 32; i++) {
        result = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
        int a11_bit = (digit << i) & a11;
        int a12_bit = (digit << i) & a12;
        int b1_bit = (digit << i) & b1;
        
        if(b1_bit == 0) {
            if(a12_bit == 0) {
                if(a11_bit == 1) {
                    result = {{0, 0}, {0, 1}, {-1, -1}, {-1, -1}};
                }
            }
            else {
                if(a11_bit == 0) {
                    result = {{0, 0}, {-1, -1}, {1, 0}, {-1, -1}};
                }
                else {
                    result = {{0, 0}, {-1, -1}, {-1, -1}, {1, 1}};
                }
            }
        }
        else {
            if(a12_bit == 0) {
                if(a11_bit == 0) {
                    printf("NO");
                    break;
                }
                else {
                    result = {{-1, -1}, {-1, -1}, {1, 0}, {1, 1}};
                }
            }
            else {
                if(a11_bit == 0) {
                    result = {{-1, -1}, {0, 1}, {-1, -1}, {1, 1}};
                }
                else {
                    result = {{-1, -1}, {0, 1}, {0, 1}, {-1, -1}};
                }
            }
        }
    }
}
