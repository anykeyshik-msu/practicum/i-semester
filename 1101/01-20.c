#include <stdio.h>

int main(void) {
    double a, b, c, d;
    scanf("%lf %lf %lf %lf", &a, &b, &c, &d);
    
    if(c < d) {
        double temp = c;
        c = d;
        d = temp;
    }
    if(a < b) {
        double temp = a;
        a = b;
        b = temp;
    }

    if( (c <= a) && (d <= b) ) {
        printf("YES");
    }
    else {
        double test = (a + b) / (c + d) * (a + b) / (c + d) + (a - b) / (c - d) * (a - b) / (c - d);
        if( (test >= 2) && (c > a) && (d <= b) ) {
            printf ("YES");
        }
        else {
            printf("NO");
        }
    }
}
