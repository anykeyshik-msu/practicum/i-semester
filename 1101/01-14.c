#include <stdio.h>
#include <stdint.h>

int main(void) {
	long n;
	long long cur, min = INT32_MAX, max = INT32_MIN, second;
	long long resl = 1, resr = INT64_MIN;

	scanf("%ld", &n);
	for (long i = 0; i != n; ++i) {
		scanf("%lld", &cur);
		if (cur * max > cur * min) {
			second = max;
		} else {
			second = min;
		}

		if (cur > max) {
			max = cur;
		}
		if (cur < min) {
			min = cur;
		}

		if (i == 0) {
			continue;
		}
		if (cur * second > resl * resr || (cur * second == resl * resr && cur + second < resl + resr)) {
			resl = cur;
			resr = second;
		}

	}

	if (resl > resr) {
		resl ^= resr;
		resr ^= resl;
		resl ^= resr;
	}

	printf("%lld %lld\n", resl, resr);
}
