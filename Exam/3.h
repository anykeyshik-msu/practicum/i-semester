#include <stdio.h>
#include <stdlib.h>

struct list {
	int key;
	struct list *next;
	struct list *sublist_head;
};

typedef struct pair {
    int key;
    int len;
} pair_t;

pair_t *
branch_check(struct list *);

int
cmp(pair_t *, pair_t *);

int 
examine(struct list *garland)
{
    int ans;
    pair_t *pair;

    pair = branch_check(garland);
    ans = pair->key;

    free(pair);
    return ans;
}

pair_t *
branch_check(struct list *garland)
{
    pair_t *ans;
    pair_t *child;
    pair_t *temp;

    ans = calloc(1, sizeof(pair_t));
    ans->key = -1;
    child = calloc(1, sizeof(pair_t));

    while (garland != NULL) {
        if (ans->key > garland->key) {
            ans->key = garland->key;
        }

        ans->len++;
        garland = garland->next;

        temp = branch_check(garland->sublist_head);

        if (cmp(child, temp) > 0) {
            free(child);
            child = temp;
        }
    }

    if (cmp(ans, child) > 0) {
        free(ans);
        ans = child;
    }

    return ans;
}

int
cmp(pair_t *first, pair_t *second)
{
    if (first->len > second->len) {
        return -1;
    } else if(first->len == second->len) {
        return 0;
    }

    return 1;
}
