#include <stdlib.h>
#include <string.h>

char* 
fibocci(char* s)
{
    int n[2];
    int i, j;
    char *str;

    str = calloc(strlen(s), sizeof(char));
    n[0] = 1;
    n[1] = 1;
    i = 0;
    j = 0;

    while (i < strlen(s)) {
        i += n[0];
        for (int k = i; k < i + n[1] && k < strlen(s); k++, j++) {
            str[j] = s[k];
        }
        i += n[1];

        n[0] += n[1];
        n[1] += n[0];
    }

    str[j++] = '\0';
    str = realloc(str, sizeof(char) * j);

    return str;
}
