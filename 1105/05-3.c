#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int *array;
    int array_size, offset;
    int input;

    array_size = 10;
    offset = 0;
    array = calloc(array_size, sizeof(int));

    scanf("%d", &input);
    while (input != 0) {
        array[offset] = input;

        offset++;
        if (offset >= array_size) {
            array_size *= 2;
            array = realloc(array, array_size * sizeof(int));
        }

        scanf("%d", &input);
    }

    for(int i = 0; i < offset; i += 2) {
        printf("%d ", array[i]);
    }
    for(int i = 1; i < offset; i += 2) {
        printf("%d ", array[i]);
    }

    free(array);
    return 0;
}
