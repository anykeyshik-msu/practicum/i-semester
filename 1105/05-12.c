#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
swap(long long **, long long **);

void
gauss(long long **, int);

void
back_substitution(long long **, long long *, int);

int
main(void)
{
    long long **matrix;
    long long *ans;
    int size;

    scanf("%d", &size);

    matrix = calloc(size, sizeof(long long *));
    for (int i = 0; i < size; i++) {
        matrix[i] = calloc(size + 1, sizeof(long long));

        for(int j = 0; j < size + 1; j++) {
            scanf("%lld", &matrix[i][j]);
        }
    }
    gauss(matrix, size);

#if DEBUG
    printf("\n===============\n");
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size + 1; j++) {
            printf("%lld ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("===============\n\n");
#endif

    ans = calloc(size, sizeof(long long));
    back_substitution(matrix, ans, size);

    for (int i = 0; i < size; i++) {
        printf("%lld\n", ans[i]);
    }

    for (int i = 0; i < size; i++) {
        free(matrix[i]);
    }    
    free(matrix);
    free(ans);

    return 0;
}

void
gauss(long long **matrix, int size)
{
    for (int i = 0; i < size; i++) {
        if (matrix[i][i] == 0) {
            int j = 0;
            while ((i + j) < size - 1 && matrix[i + j][i] == 0) {
                j++;
            }
            swap(&matrix[i], &matrix[i + j]);
        }

        for (int j = 0; j < size; j++) {
            if (i != j) {
                long long denom = matrix[j][i];
                for (int k = 0; k < size + 1; k++) {
                    matrix[j][k] = matrix[j][k] * matrix[i][i] - matrix[i][k] * denom;
                }
            }
        }
    }
}

void
back_substitution(long long **matrix, long long *ans, int size)
{
    for (int i = size - 1; i >= 0; i--) {
        long long sum = 0;
        for(int j = 0; j < size; j++) {
            sum += matrix[i][j] * ans[j];
        }

        ans[i] = (matrix[i][size] - sum) / matrix[i][i];
    }
}

void
swap(long long **first, long long **second)
{
    long long *temp = *first;
    *first = *second;
    *second = temp;
}
