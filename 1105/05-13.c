#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
find_way(int **, int **, int, 
        int, int,
        int, int);

int
main(void)
{
    int **adj_list;
    int **path;
    int n, m, n0, m0, n1, m1;
    int start_height;

    scanf("%d %d %d %d %d %d", &n, &m, &n0, &m0, &n1, &m1);

    adj_list = calloc(n, sizeof(int *));
    for (int i = 0; i < n; i++) {
        adj_list[i] = calloc(m, sizeof(int));

        for(int j = 0; j < m; j++) {
            scanf("%d", &adj_list[i][j]);
        }
    }

    start_height = adj_list[n0][m0];
    
    path = malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++) {
        path[i] = malloc(m * sizeof(int));
        memset(path[i], -1, m * sizeof(int));
    }
    path[n0][m0] = 0;

    find_way(adj_list, path, start_height,
            n, m,
            n0, m0);

    printf("%d", path[n1][m1]);

    for (int i = 0; i < n; i++) {
        free(adj_list[i]);
        free(path[i]);
    }
    free(adj_list);
    free(path);

    return 0;
}

void
find_way(int **adj_list, int **path, int current_height, 
        int n, int m,
        int x, int y)
{
    if (x - 1 >= 0
            && ((path[x - 1][y] != -1 && abs(adj_list[x - 1][y] - current_height) + path[x][y] < path[x - 1][y])
            || path[x - 1][y] == -1)
            ) {
        path[x - 1][y] = abs(adj_list[x - 1][y] - current_height) + path[x][y];

        find_way(adj_list, path, adj_list[x - 1][y], n, m, x - 1, y);
    }

    if (y - 1 >= 0 
            && ((path[x][y - 1] != -1 && abs(adj_list[x][y - 1] - current_height) + path[x][y] < path[x][y - 1])
            || path[x][y - 1] == -1)
            ) {
        path[x][y - 1] = abs(adj_list[x][y - 1] - current_height) + path[x][y];

        find_way(adj_list, path, adj_list[x][y - 1], n, m, x, y - 1);
    }

    if (x + 1 < n
            && ((path[x + 1][y] != -1 && abs(adj_list[x + 1][y] - current_height) + path[x][y] < path[x + 1][y])
            || path[x + 1][y] == -1)
            ) {
        path[x + 1][y] = abs(adj_list[x + 1][y] - current_height) + path[x][y];

        find_way(adj_list, path, adj_list[x + 1][y], n, m, x + 1, y);
    }

    if (y + 1 < m 
            && ((path[x][y + 1] != -1 && abs(adj_list[x][y + 1] - current_height) + path[x][y] < path[x][y + 1])
            || path[x][y + 1] == -1)
            ) {
        path[x][y + 1] = abs(adj_list[x][y + 1] - current_height) + path[x][y];

        find_way(adj_list, path, adj_list[x][y + 1], n, m, x, y + 1);
    }
}
