#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int
is_prime(int);

void
hyper_prime(int, int, int);

int
main(void)
{
    int N;

    scanf("%d", &N);

    hyper_prime(N, 0, 0);

    return 0;
}

int
is_prime(int n)
{
    if (n < 2) {
        return 0;
    }

    for (int i = 2; i <= sqrt(n); i++) {
        if (n % i == 0) {
            return 0;
        }
    }

    return 1;
}

void
hyper_prime(int N, int size, int number) 
{
    if (size == N) {
        printf("%d ", number);
    }
    for (int i = 0; i < 10; i++) {
        if ( is_prime(number * 10 + i) ) {
            hyper_prime(N, size + 1, number * 10 + i);
        }
    }
}
