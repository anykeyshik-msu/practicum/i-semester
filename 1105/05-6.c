#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int
main(void)
{
    int L;
    int flag;
    char *str;
    char sym;

    scanf("%d", &L);
    str = calloc(L / 2, sizeof(char));
    flag = 1;

    for (int i = 0; i < L / 2; i++) {
        scanf(" %c", &str[i]);
    }
    if (L % 2 != 0) {
        scanf(" %c", &sym);
    }
    for (int i = L / 2 - 1; i >= 0; i--) {
        scanf(" %c", &sym);
        if (str[i] != sym) {
            flag = 0;
            break;
        }
    }

    if (flag) {
        printf("YES");
    } else {
        printf("NO");
    }


    free(str);
    return 0;
}
