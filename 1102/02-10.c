#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int
main(void) 
{
    int n;
    double a, v0, v, t;
    double *s;

    v0 = 0;
    t = 0;
    scanf("%d", &n);
    s = (double *) malloc(sizeof(double) * n);
    for (int i = 0; i < n; i++) {
        scanf("%lf", &s[i]);
    }

    for (int i = 0; i < n; i++) {
        scanf("%lf", &a);

        if (fabs(a) > 0) {
            v = sqrt(v0 * v0 + 2 * a * s[i]);
            t += (v - v0) / a;
            v0 = v;
        } else {
            #if DEBUG
            fprintf(stderr, "Zero accel\nv = %lf\n", v0);
            #endif
            t += s[i] / v0;
        }
    }

    printf("%.4lf", t);

    free(s);

    return 0;
}
