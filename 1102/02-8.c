#include <stdio.h>
#include <stdlib.h>

int 
main(void) 
{
    int *array;
    int n;
    int counter;

    counter = 0;
    scanf("%d", &n);
    array = calloc(n, sizeof(int));
    for (int i = 0; i < n; i++) {
        scanf("%d", &array[i]);
    }

    for (int i = 0; i < n - 2; i++) {
        for (int k = i + 1; k < n - 1; k++) {
            int j = 2 * k - i;

            if (j < n) {
                if (array[k] * 2 == array[i] + array[j]) {
                    counter++;
                }
            }   
        }
    }

    printf("%d", counter);

    free(array);

    return 0;
}
