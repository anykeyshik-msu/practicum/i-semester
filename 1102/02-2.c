#include <stdio.h>
#include <stdlib.h>

int
main(void) 
{
    int *array;
    int array_size;
    int input;
    int index;
    unsigned char parity;

    array_size = 10;
    array = (int *) calloc(array_size, sizeof(int));
    index = 0;
    parity = 0;
    scanf("%d", &input);

    while (input != 0) {
        if (parity) { 
            array[index] = input;
            index++;

            if (index == array_size) {
                array_size *= 2;

                array = (int *) realloc(array, sizeof(int) * array_size);
            }
        } else {
            printf("%d ", input);
        }
        
        parity ^= 1;
        scanf("%d", &input);
    }

    for (int i = --index; i >= 0; i--) {
        printf("%d ", array[i]);
    }

    free(array);

    return 0;
}
