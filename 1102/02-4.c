#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int n;
    int *array;
    int input;
    int sum;

    scanf("%d", &n);
    sum = 0;
    array = (int *) calloc(n, sizeof(int));

    for (int i = 0; i < n; i++) {
        scanf("%d", &array[i]);
    }
    for (int i = 0; i < n; i++) {
        scanf("%d", &input);

        sum += (array[i] > input ? array[i] : input);
    }

    printf("%d", sum);

    free(array);

    return 0;
}
