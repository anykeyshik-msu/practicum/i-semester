#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int 
main(void) 
{
    int SHIFT = 3;

    int *array;
    int *buf;
    int *result;
    int n;

    scanf("%d", &n);
    array = calloc(n, sizeof(int));
    buf = calloc(n, sizeof(int));
    result = calloc(n, sizeof(int));

    for (int i = 0; i < n; i++) {
        scanf("%d", &array[i]);
    }
    memcpy(buf, array, sizeof(int) * n);
    memcpy(result, array, sizeof(int) * n);

    for (int j = 1; j < SHIFT; j++) {
        for(int i = 0; i < n; i++) {
            buf[i] = result[array[i] - 1];
        }
        memcpy(result, buf, sizeof(int) * n);
    }

    for (int i = 0; i < n; i++) {
        printf("%d ", result[i]);
    }

    free(array);
    free(buf);
    free(result);

    return 0;
}
