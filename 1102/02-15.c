#include <stdio.h>
#include <stdlib.h>

int 
main(void)
{
    unsigned int *array;
    int n;
    unsigned int mask = 1 << 31;
    unsigned int result = 0;
    unsigned int a = 0;
    unsigned int b = 0;

    scanf("%d", &n);

    array = (unsigned int *) calloc(n, sizeof(unsigned int));

    for (int i = 0; i < n; i++) {
        scanf("%d", &array[i]);
        result ^= array[i];
    }

    while (result < mask) {
        mask >>= 1;
    }

    for (int i = 0; i < n; i++) {
        if (mask & array[i]) {
            a ^= array[i];
        } else {
            b ^= array[i];
        }
    }

    if (a < b) {
        printf("%d %d", a, b);
                    
    } else {
        printf("%d %d", b, a);                
    }

    free(array);
    
    return 0;
}
