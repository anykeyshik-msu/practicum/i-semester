#include <stdio.h>
#include <stdlib.h>

int min(int, int);
int max(int, int);

int 
main(void)
{
    int *col, *row, **ans;
    int max;
    int sum = 0;

    col = (int *) calloc(8, sizeof(int));
    row = (int *) calloc(8, sizeof(int));
    ans = (int **) calloc(8, sizeof(int *));
    for (int i = 0; i < 8; i++) {
        ans[i] = (int *) calloc(8, sizeof(int));

        scanf("%d", &col[i]);
    }
    for (int i = 0; i < 8; i++) {
        scanf("%d", &row[i]);

        for (int j = 0; j < 8; j++) {
            ans[i][j] = min(row[i], col[j]);
        }
    }

    for (int i = 0; i < 8; i++) {
        max = ans[i][0];
        for (int j = 0; j < 8; j++) {
            if (ans[i][j] > max) {
                max = ans[i][j];
            }
            sum += ans[i][j];
        }

        if (max < col[i]) {
            sum = -1;
            break;
        }
    }

    if (sum != -1) {
        for (int i = 0; i < 8; i++) {
            max = ans[0][i];
            for (int j = 0; j < 8; j++) {
                if (ans[j][i] > max) {
                    max = ans[j][i];
                }
            }
        
            if (max < row[i]) {
                sum = -1;
                break;
            } 
        }
    }

    printf("%d", sum);

    return 0;
}

int 
min(int a, int b) 
{
    return a > b ? b : a;
}
